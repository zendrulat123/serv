module gitlab.com/zendrulat123/serv

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/rs/cors v1.7.0 // indirect
	gorm.io/driver/mysql v1.1.0 // indirect
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/gorm v1.21.10 // indirect
)
